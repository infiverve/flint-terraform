flint-terraform
===============

Provision and Decommision virtual machines of different cloud providers using Terraform.

#### What is Flint IT Automation Patform?

Flint is a lean IT Process Automation & Orchestration Platform for IT infrastructure and applications. It empowers teams to leverage existing scripting (Ruby or Groovy) skills to develop powerful workflows and processes which can be published as microservices.

Flint helps in automation of routine IT tasks & activities which saves time and cost thus allowing the team to focus on strategic initiatives and innovation. Know more: http://www.getflint.io

#### What is Terraform?
Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.
Know more: https://terraform.io/

## Add new Flintbox from Flint Console
* Go to flintbox
* Click on Add Flintbox
* set Git url of this repo 
* Click add
* Enable the flintbox

## Global Configuration in flint

#### Name: images
#### Sample config JSON
```json
{
  "aws": {
    "default": {
      "linux": {
        "ami": "xxxx",
        "availability_zone": "us-east-1a",
        "instance_type": "t1.micro",
        "region": "us-east-1",
        "subnet_id": "xxxx"
      },
      "ubuntu": {
        "ami": "ami-d85e75b0",
        "availability_zone": "us-east-1a",
        "instance_type": "t1.micro",
        "region": "us-east-1",
        "subnet_id": ""
      },
      "windows": {
        "ami": "xxxx",
        "availability_zone": "us-east-1a",
        "instance_type": "t1.micro",
        "region": "us-east-1",
        "subnet_id": "xxxx"
      }
    },
    "us-east-1": {
      "linux": {
        "us-east-1a": "xxxxxxxxxxxxx"
      },
      "ubuntu": {
        "us-east-1b": "xxxxxxxxxxxxx"
      },
      "windows": {
        "us-east-1c": "xxxxxxxxxxxxx"
      }
    },
    "us-west-1": {}
  },
  "googlecompute": {
    "centos-cloud": "centos-xxxx",
    "debian-cloud": "debian-xxxx",
    "ubuntu": "ubuntu-xxxx",
    "default": {
      "ubuntu": {
        "image": "ubuntu-1404-trusty-v20150127",
        "machine_type": "n1-standard-1",
        "region": "us-central1",
        "zone": "us-central1-a"
      },
      "centos-cloud": {
        "image": "centos-6-v20140924",
        "machine_type": "n1-standard-1",
        "region": "us-central1",
        "zone": "us-central1-a"
      },
      "debian-cloud": {
        "image": "debian-7-wheezy-v20131014",
        "machine_type": "n1-standard-1",
        "region": "us-central1",
        "zone": "us-central1-a"
      }
    }
  },
  "digitalocean": {
    "ubuntu": "ubuntu-14-04-x64",
    "default": {
      "ubuntu": {
        "region": "NYC2",
        "image": "ubuntu-14-04-x64",
        "size": "512mb"
      }
    }
  }
}
``` 
Note: You can configure the OS of your choice as per the availability in respected cloud provider.

### Global configuration for Terraform
#### Name: terraform
#### Config JSON
```json
{
  "dir": "PATH_TO_DIRECTORY_WHERE_TERRAFORM_TEMPLATES_ARE_PRESENT",
  "aws_key_file_dir": "PATH_TO_AWS_KEY_FILE",
  "ssh_connector": {
    "hostname": "192.168.2.1",
    "name": "xxxx",
    "password": "xxxx",
    "username": "xxxx"
  }
}
```

| Name | Description          |
| ------------- | ----------- |
| dir      | Full path to a directory where terraform templates are present|
| hostname     | Name of SSH connector which is enabeled on Flint |
| name | Username to login into machine |
| password | Password for that user |


## How to use it from Flintbits
## Example
### Provision virtual machine on AWS
#### Ruby
```ruby 
# call the flintbit to provision machine on AWS
@result = @call.bit("flint-terraform:aws:provision.rb")
			 .set("region","us-east-1")
			 .set("availability_zone","us-east-1a")
			 .set("instance_type","t1.micro")
			 .set("os","ubuntu")
			 .set("request_id","vm-1234")
			 .set("timeout", 300000)	# Timeout in ms
			 .sync

# Get result of flintbit execution
flintbit_exitcode = @result.exitcode
flintbit_message = @result.message

# Check if flintbit runs successfully
if flintbit_exitcode == 0
	# Check if virtual machine created successfully
	if @result.get("exitcode")
		# Getting state/info of virtual machine
		@state_of_machine = @result.get("state")
	else
		error_message = @result.get("error")
		# Log the error message while provisioning virtual machine
		@log.error("Error while provisioning vm : "+error_message)
	end
else
	# Log the error message while executing flintbit
	@log.error("Error while executing flintbit : "+flintbit_message)
end
```

### Decommission virtual machine on AWS
#### Ruby
```ruby 
# call the flintbit to destroy machine on AWS
@result = @call.bit("flint-terraform:aws:destroy.rb")
			 .set("region","us-east-1")
			 .set("instance_id","vm-1234")
			 .set("timeout", 180000)	# Timeout in ms
			 .sync

# Get result of flintbit execution
flintbit_exitcode = @result.exitcode
flintbit_message = @result.message

# Check if flintbit runs successfully
if flintbit_exitcode == 0
	# Check if virtual machine destroyed successfully
	if @result.get("exitcode")
		message = @result.get("message")
	else
		error_message = @result.get("error")
		# Log the error message while destroying virtual machine
		@log.error("Error while destroying vm : "+error_message)
	end
else
	# Log the error message while executing flintbit
	@log.error("Error while executing flintbit : "+flintbit_message)
end
```

### Provision virtual machine on Google Cloud
#### Ruby
```ruby 
# call the flintbit to provision machine on Google Cloud
@result = @call.bit("flint-terraform:googlecpmpute:provision.rb")
            .set("region","us-central1")
            .set("zone","us-central1-f")
            .set("machine_type","n1-standard-1")
            .set("os","ubuntu")
            .set("request_id","vm-1234")
            .set("timeout", 300000) # Timeout in ms
            .sync

# Get result of flintbit execution
flintbit_exitcode = @result.exitcode
flintbit_message = @result.message

# Check if flintbit runs successfully
if flintbit_exitcode == 0
  # Check if virtual machine created successfully
  if @result.get("exitcode")
    # Getting state/info of virtual machine
    @state_of_machine = @result.get("state")
  else
    error_message = @result.get("error")
    # Log the error message while provisioning virtual machine
    @log.error("Error while provisioning vm : "+error_message)
  end
else
  # Log the error message while executing flintbit
  @log.error("Error while executing flintbit : "+flintbit_message)
end
```

### Decommission virtual machine on Google Cloud
#### Ruby
```ruby 
# call the flintbit to destroy machine on Google Cloud
@result = @call.bit("flint-terraform:googlecompute:destroy.rb")
       .set("region","us-central1")
       .set("instance_id","vm-1234")
       .set("timeout", 180000)  # Timeout in ms
       .sync

# Get result of flintbit execution
flintbit_exitcode = @result.exitcode
flintbit_message = @result.message

# Check if flintbit runs successfully
if flintbit_exitcode == 0
  # Check if virtual machine destroyed successfully
  if @result.get("exitcode")
    message = @result.get("message")
  else
    error_message = @result.get("error")
    # Log the error message while destroying virtual machine
    @log.error("Error while destroying vm : "+error_message)
  end
else
  # Log the error message while executing flintbit
  @log.error("Error while executing flintbit : "+flintbit_message)
end
```

### Provision virtual machine on Digital Ocean
#### Ruby
```ruby 
# call the flintbit to provision machine on Digital Ocean
@result = @call.bit("flint-terraform:digitalocean:provision.rb")
            .set("region","nyc2")
            .set("os","ubuntu")
            .set("size","512mb")
            .set("token","xxxxxxxxxxxxxxxxx")
            .set("request_id","vm-1234")
            .set("timeout", 300000) # Timeout in ms
            .sync

# Get result of flintbit execution
flintbit_exitcode = @result.exitcode
flintbit_message = @result.message

# Check if flintbit runs successfully
if flintbit_exitcode == 0
  # Check if virtual machine created successfully
  if @result.get("exitcode")
    # Getting state/info of virtual machine
    @state_of_machine = @result.get("state")
  else
    error_message = @result.get("error")
    # Log the error message while provisioning virtual machine
    @log.error("Error while provisioning vm : "+error_message)
  end
else
  # Log the error message while executing flintbit
  @log.error("Error while executing flintbit : "+flintbit_message)
end
```

### Decommission virtual machine on Digital Ocean
#### Ruby
```ruby 
# call the flintbit to destroy machine on Digital Ocean
@result = @call.bit("flint-terraform:digitalocean:destroy.rb")
       .set("token","xxxxxxxxxxxxxxxxxxx")
       .set("instance_id","vm-1234")
       .set("timeout", 180000)  # Timeout in ms
       .sync

# Get result of flintbit execution
flintbit_exitcode = @result.exitcode
flintbit_message = @result.message

# Check if flintbit runs successfully
if flintbit_exitcode == 0
  # Check if virtual machine destroyed successfully
  if @result.get("exitcode")
    message = @result.get("message")
  else
    error_message = @result.get("error")
    # Log the error message while destroying virtual machine
    @log.error("Error while destroying vm : "+error_message)
  end
else
  # Log the error message while executing flintbit
  @log.error("Error while executing flintbit : "+flintbit_message)
end
```