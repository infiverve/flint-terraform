@log.trace("Started execution of flint-terraform:digitalocean:destroy.rb flintbit..")
# Getting input parameters
@token = @input.get("token")
@id = @input.get("instance_id")

if @token == nil || @token == ""
	@log.error("Please provide valid token")
	@output.exit(1,"Token is blank or not provided")
end

@provider = "digitalocean"

if @id == nil || @id == ""
	@log.error("Please provide valid Instance ID")
	@output.exit(1,"Instance ID is blank or not provided")
else
	# No need to implement logic to call flintbit to get state of instance
	
	@destroy_command = "terraform destroy"
	@destroy_command = @destroy_command + " -var 'region='" \
									" -var 'do_token=#{@token}'" \
									" -var 'size='" \
									" -var 'image='" \
									" -var 'name='" \
									" -state='#{@id}.json' -force"
	@log.debug("Terraform destroy command : #{@destroy_command}")
	
	flintbit_response = @call.bit("flint-terraform:common:destroy.rb")
							.set("destroy-command",@destroy_command)
							.set("provider",@provider)
							.timeout(180000)
							.sync
	
	flintbit_exitcode = flintbit_response.exitcode
	flintbit_message = flintbit_response.message
	
	if flintbit_exitcode == 0
		if flintbit_response.get("exitcode") == 0
			@message = flintbit_response.get("message")
			@log.info("Successfully destroyed vm..")
			@output.set("message",@message)
			@output.set("exitcode",0)
		else
			error_message = flintbit_response.get("error")
			@log.error("Error while destroying vm : " + error_message)
			@output.set("error",error_message)
			@output.set("exitcode",1)
		end
	else
		error_message = "Error while executing flintbit to destroy an virtual machine : #{flintbit_message}"
		@output.set("exitcode",1)
		@output.set("error",error_message)
		@log.error(error_message)
	end
end

@log.trace("Finished execution of flint-terraform:digitalocean:destroy.rb flintbit..")