@log.trace("Started execution of flint-terraform:digitalocean:provision.rb flintbit..")

# Getting input parameters
@request_id = @input.get("request_id")
@region = @input.get("region")
@size = @input.get("size")
@token = @input.get("token")
@os = @input.get("os")

@provider = "digitalocean"
@image = ""

if @request_id == nil || @request_id == ""
	@log.error("Request ID is blank or not provided")
	@output.exit(1,"Request ID is blank or not provided")
end

if @os == nil || @os == ""
	@log.error("Please provide valid OS name")
	@output.exit(1,"OS name is blank or not provided")
end

if @token == nil || @token == ""
	@log.error("Please provide valid token")
	@output.exit(1,"Token is blank or not provided")
end

if @region == nil || @region == ""
	@region = @config.global("images.#{@provider}.default.#{@os}.region")
	if @region == nil || @region == ""
		@log.error("Default region is blank or not provided")
		@output.exit(1,"Default region is blank or not provided")
	end
	@log.debug("Region is not provided so using default region : #{@region}")
	@image = @config.global("images.#{@provider}.default.#{@os}.image")
	@size = @config.global("images.#{@provider}.default.#{@os}.size")
else
	@region.downcase!
	if @size == nil || @size == ""
		@log.error("Size is blank or not provided")
		@output.exit(1,"Size is blank or not provided")
	end

	@image = @config.global("images.#{@provider}.#{@os}")
	if @image == nil || @image == ""
		@log.error("Please configure image for given os : #{@os}")
		@output.exit(1,"Please configure image for given os : #{@os}")
	end
end

@log.info("Request ID : #{@request_id} | Region : #{@region} | Size : #{@size} | Image : #{@image}")

@apply_command = "terraform apply"
@apply_command = @apply_command + " -var 'region=#{@region}'" \
								" -var 'do_token=#{@token}'" \
								" -var 'size=#{@size}'" \
								" -var 'image=#{@image}'" \
								" -var 'name=#{@request_id}'" \
								" -state='#{@request_id}.json'"

@state_command = "cat #{@request_id}.json"

flintbit_response = @call.bit("flint-terraform:common:apply.rb")
						.set("apply-command",@apply_command)
						.set("state-command",@state_command)
						.set("provider",@provider)
						.timeout(180000)
						.sync

flintbit_exitcode = flintbit_response.exitcode
flintbit_message = flintbit_response.message

if flintbit_exitcode == 0
	if flintbit_response.get("exitcode") == 0
		@state_of_machine = flintbit_response.get("state")
		@log.info("Successfully created machine with below details")
		@log.info("State of machine : #{@state_of_machine}")
		@name = @state_of_machine['name']
		@instance_id = @state_of_machine['id']
		@output.set("state",@state_of_machine)
		# Changing name of state file
		@call.bit("flint-terraform:common:change_file_name.rb")
			.set("old_name",@name)
			.set("new_name",@instance_id)
			.set("provider",@provider)
			.async

		@output.set("exitcode",0)
	else
		error_message = flintbit_response.get("error")
		@log.error(error_message)
		@output.set("error",error_message)
		@output.set("exitcode",1)
	end
else
	error_message = "Error while executing flintbit to provision an virtual machine : #{flintbit_message}"
	@output.set("exitcode",1)
	@output.set("error",error_message)
	@log.error(error_message)
end

@log.trace("Finished execution of flint-terraform:digitalocean:provision.rb flintbit..")