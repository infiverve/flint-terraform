@log.trace("Started execution of flint-terraform:aws:provision.rb flintbit..")

# Getting input parameters
@region = @input.get("region")
@availability_zone = @input.get("availability_zone")
@instance_type = @input.get("instance_type")
@subnet_id = @input.get("subnet_id")
@os = @input.get("os")
@id = @input.get("request_id")
@provider = "aws"
@ami = ""

if @id == nil || @id == ""
	@log.error("Please provide valid request_id")
	@output.exit(1,"Request ID is blank or not provided")
end

if @os == nil || @os == ""
	@log.error("Please provide valid OS name")
	@output.exit(1,"OS name is blank or not provided")
end

@os.downcase!

# Get default configuration if region is not provided
if @region == nil || @region == ""
	@region = @config.global("images.#{@provider}.default.#{@os}.region")
	@log.debug("Region is not provided so using default region : #{@region}")
	@availability_zone = @config.global("images.#{@provider}.default.#{@os}.availability_zone")
	@ami = @config.global("images.#{@provider}.default.#{@os}.ami")
	@instance_type = @config.global("images.#{@provider}.default.#{@os}.instance_type")
	@subnet_id = @config.global("images.#{@provider}.default.#{@os}.subnet_id")
else
	if @availability_zone == nil || @availability_zone == ""
		@log.error("Please provide valid availability_zone")
		@output.exit(1,"Availability zone is blank or not provided")
	end

	if @instance_type == nil || @instance_type == ""
		@log.error("Please provide valid instance_type")
		@output.exit(1,"Instance type is blank or not provided")
	end

	@ami = @config.global("images.#{@provider}.#{@region}.#{@os}.#{@availability_zone}")
end

@log.info("Region : #{@region} | Availability zone : #{@availability_zone} | Instance type : #{@instance_type} | OS : #{@os} | AMI : #{@ami}")

@apply_command = "terraform apply"
@apply_command = @apply_command + " -var 'region=#{@region}'" \
								" -var 'availability_zone=#{@availability_zone}'" \
								" -var 'instance_type=#{@instance_type}'" \
								" -var 'ami=#{@ami}'" \
								" -var 'subnet_id=#{@subnet_id}'" \
								" -state='#{@id}.json'"

@state_command = "cat #{@id}.json"

flintbit_response = @call.bit("flint-terraform:common:apply.rb")
						.set("apply-command",@apply_command)
						.set("state-command",@state_command)
						.set("provider",@provider)
						.timeout(240000)
						.sync

flintbit_exitcode = flintbit_response.exitcode
flintbit_message = flintbit_response.message

if flintbit_exitcode == 0
	if flintbit_response.get("exitcode") == 0
		@state_of_machine = flintbit_response.get("state")
		@log.info("Successfully created machine with below details")
		@log.info("State of machine : #{@state_of_machine}")
		@instance_id = @state_of_machine['id']
		@output.set("state",@state_of_machine)
		# Changing name of state file
		@call.bit("flint-terraform:common:change_file_name.rb")
			.set("old_name",@id)
			.set("new_name",@instance_id)
			.set("provider",@provider)
			.async

		@key_name = @state_of_machine['key_name']
		@target = @state_of_machine['public_ip']

		# Calling flintbit to install apache2
#		@call.bit("flint-terraform:common:install_apache2.rb")
#			.set("provider",@provider)
#			.set("target",@target)
#			.set("key-file",@key_name)
#			.set("os",@os)
#			.async

		@output.set("exitcode",0)
	else
		error_message = flintbit_response.get("error")
		@log.error(error_message)
		@output.set("error",error_message)
		@output.set("exitcode",1)
	end
else
	error_message = "Error while executing flintbit to provision an virtual machine : #{flintbit_message}"
	@output.set("exitcode",1)
	@output.set("error",error_message)
	@log.error(error_message)
end

@log.trace("Finished execution of flint-terraform:aws:provision.rb flintbit..")
