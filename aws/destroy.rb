@log.trace("Started execution of flint-terraform:aws:destroy.rb flintbit..")
# Getting input parameters
@region = @input.get("region")
@id = @input.get("instance_id")

if @region == nil || @region == ""
	@log.error("Please provide valid region")
	@output.exit(1,"Region is blank or not provided")
end

@provider = "aws"

if @id == nil || @id == ""
	@log.error("Please provide valid Instance ID")
	@output.exit(1,"Instance ID is blank or not provided")
else
	# TODO: Implement logic to call flintbit to get state of instance
	get_state_flintbit_call_response = @call.bit("flint-terraform:common:get_state.rb")
										.set("instance_id", @id)
										.set("provider", @provider)
										.timeout(60000)
										.sync
										
	if get_state_flintbit_call_response.exitcode == 0
		if get_state_flintbit_call_response.get("exitcode") == 0
			state = get_state_flintbit_call_response.get("state")
			@ami = state.get("ami")
			@availability_zone = state.get("availability_zone")
			@instance_type = state.get("instance_type")
			@subnet_id = state.get("subnet_id")
			@log.info("Destroying VM with below details..")
			@log.info("Region : #{@region} | Availability zone : #{@availability_zone} | Instance type : #{@instance_type} | OS : #{@os} | AMI : #{@ami}")
			
			@destroy_command = "terraform destroy"
			@destroy_command = @destroy_command + " -var 'region=#{@region}'" \
											" -var 'availability_zone=#{@availability_zone}'" \
											" -var 'instance_type=#{@instance_type}'" \
											" -var 'ami=#{@ami}'" \
											" -var 'subnet_id=#{@subnet_id}'" \
											" -state='#{@id}.json' -force"
			
			flintbit_response = @call.bit("flint-terraform:common:destroy.rb")
									.set("destroy-command",@destroy_command)
									.set("provider",@provider)
									.timeout(180000)
									.sync
			
			flintbit_exitcode = flintbit_response.exitcode
			flintbit_message = flintbit_response.message
			
			if flintbit_exitcode == 0
				if flintbit_response.get("exitcode") == 0
					@message = flintbit_response.get("message")
					@log.info("Successfully destroyed vm..")
					@output.set("message",@message)
					@output.set("exitcode",0)
				else
					error_message = flintbit_response.get("error")
					@log.error("Error while destroying vm : " + error_message)
					@output.set("exitcode",1)
					@output.set("error",error_message)
				end
			else
				error_message = "Error while executing flintbit to destroy an virtual machine : #{flintbit_message}"
				@output.set("exitcode",1)
				@output.set("error",error_message)
				@log.error(error_message)
			end
		else
			@log.error(get_state_flintbit_call_response.get("error"))
			@output.set("exitcode",1)
			@output.set("error",get_state_flintbit_call_response.get("error"))
			#@output.exit(1,get_state_flintbit_call_response.get("error"))
		end
	else
		@log.error(get_state_flintbit_call_response.message)
		@output.set("exitcode",1)
		@output.set("error",get_state_flintbit_call_response.message)
		#@output.exit(1,get_state_flintbit_call_response.message)
	end
end

@log.trace("Finished execution of flint-terraform:aws:destroy.rb flintbit..")