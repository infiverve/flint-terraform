@log.trace("Started execution of flint-terraform:googlecompute:destroy.rb flintbit..")

# Getting input parameters
@region = @input.get("region")
@instance_id = @input.get("instance_id")

#if @region == nil || @region == ""
#	@log.error("Please provide valid region")
#	@output.exit(1,"Region is blank or not provided")
#end

@provider = "googlecompute"

if @instance_id == nil || @instance_id == ""
	@log.error("Instance ID is blank or not provided")
	@output.exit(1,"Instance ID is blank or not provided")
else
	@destroy_command = "terraform destroy"
	@destroy_command = @destroy_command + " -var 'region=#{@region}'" \
									" -var 'zone='" \
									" -var 'machine_type='" \
									" -var 'image='" \
									" -var 'name=#{@instance_id}'" \
									" -state='#{@instance_id}.json' -force"
	
	flintbit_response = @call.bit("flint-terraform:common:destroy.rb")
							.set("destroy-command",@destroy_command)
							.set("provider",@provider)
							.timeout(180000)
							.sync
	
	flintbit_exitcode = flintbit_response.exitcode
	flintbit_message = flintbit_response.message
	
	if flintbit_exitcode == 0
		if flintbit_response.get("exitcode") == 0
			@message = flintbit_response.get("message")
			@log.info("Successfully destroyed vm..")
			@output.set("message",@message)
			@output.set("exitcode",0)
		else
			error_message = flintbit_response.get("error")
			@log.error("Error while destroying vm : " + error_message)
			@output.set("error",error_message)
			@output.set("exitcode",1)
		end
	else
		error_message = "Error while executing flintbit to destroy an virtual machine : #{flintbit_message}"
		@output.set("exitcode",1)
		@output.set("error",error_message)
		@log.error(error_message)
	end
end
@log.trace("Finished execution of flint-terraform:googlecompute:destroy.rb flintbit..")