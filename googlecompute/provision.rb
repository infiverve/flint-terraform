@log.trace("Started execution of flint-terraform:googlecloud:provision.rb flintbit..")

# Getting input parameters
@region = @input.get("region")
@zone = @input.get("zone")
@machine_type = @input.get("machine_type")
# This field will be treated as 'name' for instance
@request_id = @input.get("request_id")
@provider = "googlecompute"
@os = @input.get("os")
@image = ""

if @request_id == nil || @request_id == ""
	@log.error("Request ID is blank or not provided")
	@output.exit(1,"Request ID is blank or not provided")
end

if @os == nil || @os == ""
	@log.error("Please provide valid OS name")
	@output.exit(1,"OS name is blank or not provided")
end

if @region == nil || @region == ""
	#@log.error("Region is blank or not provided")
	#@output.exit(1,"Region is blank or not provided")
	@region = @config.global("images.#{@provider}.default.#{@os}.region")
	if @region == nil || @region == ""
		@log.error("Default region is blank or not provided")
		@output.exit(1,"Default region is blank or not provided")
	end
	@log.debug("Region is not provided so using default region : #{@region}")
	@zone = @config.global("images.#{@provider}.default.#{@os}.zone")
	@image = @config.global("images.#{@provider}.default.#{@os}.image")
	@machine_type = @config.global("images.#{@provider}.default.#{@os}.machine_type")
else
	if @zone == nil || @zone == ""
		@log.error("Zone is blank or not provided")
		@output.exit(1,"Zone is blank or not provided")
	end

	if @machine_type == nil || @machine_type == ""
		@log.error("Machine type is blank or not provided")
		@output.exit(1,"Machine type is blank or not provided")
	end

	@image = @config.global("images.#{@provider}.#{@os}")
	if @image == nil || @image == ""
		@log.error("Please configure image for given os : #{@os}")
		@output.exit(1,"Please configure image for given os : #{@os}")
	end
end

@log.info("Request ID : #{@request_id} | Region : #{@region} | Zone : #{@zone} | Machine type : #{@machine_type} | Image : #{@image}")

@apply_command = "terraform apply"
@apply_command = @apply_command + " -var 'region=#{@region}'" \
								" -var 'zone=#{@zone}'" \
								" -var 'machine_type=#{@machine_type}'" \
								" -var 'image=#{@image}'" \
								" -var 'name=#{@request_id}'" \
								" -state='#{@request_id}.json'"

@state_command = "cat #{@request_id}.json"

flintbit_response = @call.bit("flint-terraform:common:apply.rb")
						.set("apply-command",@apply_command)
						.set("state-command",@state_command)
						.set("provider",@provider)
						.timeout(180000)
						.sync

flintbit_exitcode = flintbit_response.exitcode
flintbit_message = flintbit_response.message

if flintbit_exitcode == 0
	if flintbit_response.get("exitcode") == 0
		@state_of_machine = flintbit_response.get("state")
		@log.info("Successfully created machine with below details")
		@log.info("State of machine : #{@state_of_machine}")
		@output.set("state",@state_of_machine)
		@output.set("exitcode",0)
	else
		error_message = flintbit_response.get("error")
		@log.error(error_message)
		@output.set("error",error_message)
		@output.set("exitcode",1)
	end
else
	error_message = "Error while executing flintbit to provision an virtual machine : #{flintbit_message}"
	@output.set("error",error_message)
	@output.set("exitcode",1)
	@log.error(error_message)
end

@log.trace("Finished execution of flint-terraform:googlecloud:provision.rb flintbit..")