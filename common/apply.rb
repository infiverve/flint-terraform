def get_google_machine_state(state_module)
	id = state_module['resources']['google_compute_instance.default']['primary']['id']
	public_ip = state_module['resources']['google_compute_instance.default']['primary']['attributes']['network_interface.0.access_config.0.nat_ip']
	zone = state_module['resources']['google_compute_instance.default']['primary']['attributes']['zone']
	@util.json("{\"id\" : \"#{id}\", \"public_ip\" : \"#{public_ip}\", \"zone\" : \"#{zone}\"}")
end

def get_aws_machine_state(state_module)
	id = state_module['resources']['aws_instance.example']['primary']['id']
	availability_zone = state_module['resources']['aws_instance.example']['primary']['attributes']['availability_zone']
	instance_type = state_module['resources']['aws_instance.example']['primary']['attributes']['instance_type']
	private_ip = state_module['resources']['aws_instance.example']['primary']['attributes']['private_ip']
	public_ip = state_module['resources']['aws_instance.example']['primary']['attributes']['public_ip']
	key_name = state_module['resources']['aws_instance.example']['primary']['attributes']['key_name']
	@util.json("{\"id\" : \"#{id}\", \"public_ip\" : \"#{public_ip}\", \"availability_zone\" : \"#{availability_zone}\",\"instance_type\" : \"#{instance_type}\", \"private_ip\" : \"#{private_ip}\", \"key_name\" : \"#{key_name}\"}")
end

def get_digitalocean_machine_state(state_module)
	primary_data = state_module['resources']['digitalocean_droplet.web']['primary']
	id = primary_data['id']
	public_ip = primary_data['attributes']['ipv4_address']
	name = primary_data['attributes']['name']
	region = primary_data['attributes']['region']
	size = primary_data['attributes']['size']
	@util.json("{\"id\" : \"#{id}\", \"name\" : \"#{name}\",\"size\" : \"#{size}\",\"public_ip\" : \"#{public_ip}\", \"region\" : \"#{region}\"}")
end

@log.trace("Started execution of flint-terraform:common:apply.rb flintbit..")

# Getting required parameters from global config
@ssh_connector_name = @config.global("terraform.ssh_connector.name")
@target = @config.global("terraform.ssh_connector.hostname")
@username = @config.global("terraform.ssh_connector.username")
@password = @config.global("terraform.ssh_connector.password")
@terraform_dir = @config.global("terraform.dir")

# Getting input parameters
@provider = @input.get("provider")
@apply_command = @input.get("apply-command")
@get_state_command = @input.get("state-command")

@log.info("Terraform apply command : #{@apply_command}")

@apply_command = "cd #{@terraform_dir}/#{@provider};#{@apply_command}"
@get_state_command = "cd #{@terraform_dir}/#{@provider};#{@get_state_command}"

connector_call = @call.connector(@ssh_connector_name)
              		.set("target",@target)
              		.set("username",@username)
              		.set("password",@password)
              		.set("type","shell")

# ssh connector call to create required machine
#apply_command_response = connector_call.set("command",@apply_command).timeout(180000).sync

apply_command_exitcode = 0
apply_command_message = "success"

if apply_command_exitcode == 0
	apply_command_response_body = %x[ #{@apply_command} ]
	@log.info("Success in executing #{@ssh_connector_name} connector, where exitcode :: #{apply_command_exitcode} |
                                                        message :: #{apply_command_message}")
	if apply_command_response_body.include? "no such file or directory"
		error_body = "Job-id : #{@input.jobid()} | Error while provisiong machine : Instance type not supported"
		@output.set("exitcode",1)
		@output.set("error", error_body)
		@log.error(apply_command_response_body)
	elsif apply_command_response_body.include? "Error loading config:"
		error_body = "Job-id : #{@input.jobid()} | Error while provisiong machine : #{apply_command_response_body}"
		@output.set("exitcode",1)
		@output.set("error",error_body)
		@log.error(apply_command_response_body)
	elsif apply_command_response_body.include? "No Terraform configuration files found"
		error_body = "Job-id : #{@input.jobid()} | Error while provisiong machine : No configuration found for this instance type"
		@output.set("exitcode",1)
		@output.set("error",error_body)
		@log.error(apply_command_response_body)
	elsif apply_command_response_body.include? "Not a valid region:"
		@output.set("exitcode",1)
		@output.set("error","Not a valid region: Please provide valid region")
	elsif apply_command_response_body.include? "Error launching source instance:"
		start_string = "Error launching source instance:"
		end_string = "status code: 400"
		error_string = apply_command_response_body[/#{start_string}(.*?)#{end_string}/m, 1]
		@output.set("exitcode",1)
		@output.set("error",error_string)
	elsif apply_command_response_body.include? "Apply complete!"
		# Successfully created machine
		@log.info("Apply command executed successfully..")
		@log.info("Terraform get state command : #{@get_state_command}")
		#get_state_command_response = connector_call.set("command",@get_state_command).set("type","exec").timeout(60000).sync

		get_state_command_exitcode = 0
		get_state_command_message = "success"

		if get_state_command_exitcode == 0
			get_state_command_response_body = %x[ #{@get_state_command} ]
			state_result = @util.json(get_state_command_response_body)
			state_module = state_result.get("modules")[0]

			@output.set("exitcode",0)
			@log.debug("Successfully get state of machine..")
			if @provider.include? "aws"
				@output.setraw("state",get_aws_machine_state(state_module).to_s)
			elsif @provider.include? "google"
				@output.setraw("state",get_google_machine_state(state_module).to_s)
			elsif @provider.include? "digitalocean"
				@output.setraw("state",get_digitalocean_machine_state(state_module).to_s)
			end
		else
			@output.set("exitcode",1)
			error_message = "Instance created successfully but error occured while getting state : #{get_state_command_message}"
			@log.error(error_message)
			@output.set("error",get_state_command_message)
		end
	else
		@output.set("exitcode",1)
		if apply_command_response_body.include? "Error applying plan:"
			apply_command_response_body = apply_command_response_body + "END"
			start_string = "Error applying plan:"
			end_string = "END"
			error_string = apply_command_response_body[/#{start_string}(.*?)#{end_string}/m, 1]
			@output.set("error",error_string)
		else
			error_body = "Job-id : #{@input.jobid()} | Error while provisiong machine : Internal error occured, please try again.."
			@log.error(apply_command_response_body)
			@output.set("error",error_body)
		end
	end
else
	@log.error("Failure in executing #{@ssh_connector_name} connector where, exitcode : #{apply_command_exitcode} |
                                                         message : #{apply_command_message}")
	error_body = "Job-id : #{@input.jobid()} | Error while provisiong machine : Internal error occured, please try again.."
	@output.set("exitcode",1)
  	@output.set("error",error_body)
end

@log.trace("Finished execution of flint-terraform:common:apply.rb flintbit..")
