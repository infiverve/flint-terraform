@log.trace("Started execution of flint-terraform:common:install_apache2.rb flintbit..")
# Getting input parameters
# TODO: Use this provider to build valid ssh connector call
@provider = @input.get("provider")
@os = @input.get("os")
@target = @input.get("target")
@key_file = @input.get("key-file")
@key_file_dir = @config.global("terraform.aws_key_file_dir")

@ssh_connector_name = "ssh"
@username = "root"
# Implement logic to change installation command as per OS type
if @os == nil || @os == ""
	@log.error("Please provide valid OS name")
	@output.exit(1,"OS name is blank or not provided")
else
	if @provider.include? "aws"
		case 
		when (@os.include? "ubuntu") || (@os.include? "linux")
			if @os.include? "ubuntu"
				@username = "ubuntu"
				@install_command = "sudo apt-get update;sudo apt-get install apache2 -y"
			else
				@username = "ec2-user"
				@install_command = "sudo yum install httpd -y;sudo /etc/init.d/httpd start"
			end
		when (@os.include? "windows")
			#TODO: Logic to install apache2 on windows machine
			@install_command = ""
		else
			@install_command = ""
		end
	elsif @provider.include? "google"
		@install_command = "whoami"
	else
		error_msg = "Unknown provider : #{@provider}", "Can not install apache2"
		@log.error(error_msg)
		@output.exit(1,error_msg)
	end
end

if @target == nil || @target == ""
	@log.error("Please provide target hostname")
	@output.exit(1,"Please provide target hostname")
end

if @username == nil || @username == ""
	@log.error("Please provide username")
	@output.exit(1,"Please provide username")
end

if @key_file == nil || @key_file == ""
	@log.error("Please provide key-file path")
	@output.exit(1,"Please provide key-file path")
else
	@key_file = @key_file_dir+@key_file+".pem"
end

@log.info("Target : #{@target} | OS : #{@os} | key-file : #{@key_file}")
@log.info("Please wait for few seconds to start installation..")
# Added sleep statement so that to wait for initialization of instance
sleep(20)
@log.info("Started installation of apache2..")
count = 0

begin
	count = count + 1
	install_command_response = @call.connector(@ssh_connector_name)
              					.set("target",@target)
              					.set("username",@username)
            	  				.set("key-file",@key_file)
        	      				.set("type","exec")
    	          				.set("command",@install_command).timeout(240000).sync

	exitcode = install_command_response.exitcode
	message = install_command_response.message
	if exitcode == 0
		@log.info("Successfully installed apache2 on #{@target}")
		@log.info("Message : #{message}")
		@output.set("exitcode",0)
		@output.set("message","Success")
		break
	else
		@log.error("Error while executing ssh connector : #{message}")
		if count < 5
			@log.debug("Retrying again..")
			sleep(15)
		else
			@log.error("Sorry!, We are unable to install apache2.")
			@output.set("exitcode",1)
			@output.set("message","Unable to install apache2 in #{count} attempts")
		end
	end
end while count < 5

@log.trace("Finished execution of flint-terraform:common:install_apache2.rb flintbit..")