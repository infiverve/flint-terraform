@log.trace("Started execution of flint-terraform:common:change_file_name.rb flintbit..")
# Getting required parameters from global config
@ssh_connector_name = @config.global("terraform.ssh_connector.name")
@target = @config.global("terraform.ssh_connector.hostname")
@username = @config.global("terraform.ssh_connector.username")
@password = @config.global("terraform.ssh_connector.password")
@terraform_dir = @config.global("terraform.dir")

# Getting input parameters
@old_name = @input.get("old_name")
@new_name = @input.get("new_name")
@provider = @input.get("provider")

ssh_command = "cd #{@terraform_dir}/#{@provider}; mv #{@old_name}.json #{@new_name}.json"

#connector_call_response = @call.connector(@ssh_connector_name)
#							.set("target",@target)
#              				.set("username",@username)
#              				.set("password",@password)
#              				.set("type","exec")
#              				.set("command",ssh_command).timeout(60000).sync
result = %x[ #{ssh_command}]

exitcode = 0
message = "success"

if exitcode == 0
	@log.info("Terraform state file name changes successfully")
	@output.set("file_name","#{@new_name}.json")
	@output.set("exitcode",0)
else
	@log.error("Error occured while changing name of state file : #{message}")
	@output.set("error","#{message}")
	@output.set("exitcode",1)
end

@log.trace("Finished execution of flint-terraform:common:change_file_name.rb flintbit..")
