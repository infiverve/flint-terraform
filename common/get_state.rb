def get_google_machine_state(state_module)
	google_instance = state_module['resources']['google_compute_instance.default']
	id, public_ip, zone = ""
	if google_instance != nil
		id = google_instance['primary']['id']
		public_ip = google_instance['primary']['attributes']['network_interface.0.address']
		zone = google_instance['primary']['attributes']['zone']
	end
	@util.json("{\"id\" : \"#{id}\", \"public_ip\" : \"#{public_ip}\", \"zone\" : \"#{zone}\"}")
end

def get_aws_machine_state(state_module)
	aws_instance = state_module['resources']['aws_instance.example']
	id, availability_zone, instance_type, private_ip, public_ip, key_name, ami = ""
	if aws_instance != nil
		id = aws_instance['primary']['id']
		availability_zone = aws_instance['primary']['attributes']['availability_zone']
		instance_type = aws_instance['primary']['attributes']['instance_type']
		private_ip = aws_instance['primary']['attributes']['private_ip']
		public_ip = aws_instance['primary']['attributes']['public_ip']
		key_name = aws_instance['primary']['attributes']['key_name']
		ami = aws_instance['primary']['attributes']['ami']
	end
	@util.json("{\"id\" : \"#{id}\", \"public_ip\" : \"#{public_ip}\", \"availability_zone\" : \"#{availability_zone}\",\"instance_type\" : \"#{instance_type}\", \"private_ip\" : \"#{private_ip}\", \"key_name\" : \"#{key_name}\", \"ami\" : \"#{ami}\"}")
end

def get_digitalocean_machine_state(state_module)
	primary_data = state_module['resources']['digitalocean_droplet.web']['primary']
	id, public_ip ,name ,region ,size = ""
	if primary_data != nil
		id = primary_data['id']
		public_ip = primary_data['attributes']['ipv4_address']
		name = primary_data['attributes']['name']
		region = primary_data['attributes']['region']
		size = primary_data['attributes']['size']
	end
	@util.json("{\"id\" : \"#{id}\", \"name\" : \"#{name}\",\"size\" : \"#{size}\",\"public_ip\" : \"#{public_ip}\", \"region\" : \"#{region}\"}")
end

@log.trace("Started execution of flint-terraform:common:get_state.rb flintbit..")
# Getting required parameters from global config
@ssh_connector_name = @config.global("terraform.ssh_connector.name")
@target = @config.global("terraform.ssh_connector.hostname")
@username = @config.global("terraform.ssh_connector.username")
@password = @config.global("terraform.ssh_connector.password")
@terraform_dir = @config.global("terraform.dir")

@provider = @input.get("provider")
@instance_id = @input.get("instance_id")

@get_state_command = "cd #{@terraform_dir}/#{@provider};cat #{@instance_id}.json"

connector_call = @call.connector(@ssh_connector_name)
              		.set("target",@target)
              		.set("username",@username)
              		.set("password",@password)
#get_state_command_response = connector_call.set("command",@get_state_command).set("type","exec").timeout(60000).sync

get_state_command_exitcode = 0
get_state_command_message = "success"

if get_state_command_exitcode == 0
	result = %x[ #{@get_state_command} ]
	if (result.length < 1) || (result.include? "No such file or directory")
		@output.set("exitcode",1)
		@output.set("error","Can not destroy VM : Unable to find state file")
	else
		state_result = @util.json(result)
		state_module = state_result.get("modules")[0]
		@output.set("exitcode",0)
		@log.debug("Successfully get state of machine..")
		if @provider.include? "aws"
			@output.setraw("state",get_aws_machine_state(state_module).to_s)
		elsif @provider.include? "google"
			@output.setraw("state",get_google_machine_state(state_module).to_s)
		elsif @provider.include? "digitalocean"
			@output.setraw("state",get_digitalocean_machine_state(state_module).to_s)
		end
	end
else
	@output.set("exitcode",1)
	error_message = "Error occured while getting state : #{get_state_command_message}"
	@log.error(error_message)
	@output.set("error",get_state_command_message)
end

@log.trace("Finished execution of flint-terraform:common:get_state.rb flintbit..")
