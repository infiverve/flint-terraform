@log.trace("Started execution of flint-terraform:common:destroy.rb flintbit..")

# Getting required parameters from global config
@ssh_connector_name = @config.global("terraform.ssh_connector.name")
@target = @config.global("terraform.ssh_connector.hostname")
@username = @config.global("terraform.ssh_connector.username")
@password = @config.global("terraform.ssh_connector.password")
@terraform_dir = @config.global("terraform.dir")

# Getting input parameters
@provider = @input.get("provider")
@destroy_command = @input.get("destroy-command")
@log.info("Terraform destroy command : #{@destroy_command}")

@destroy_command = "cd #{@terraform_dir}/#{@provider};#{@destroy_command}"

connector_call = @call.connector(@ssh_connector_name)
              		.set("target",@target)
              		.set("username",@username)
              		.set("password",@password)
              		.set("type","shell")

# ssh connector call to destroy required machine
#destroy_command_response = connector_call.set("command",@destroy_command).timeout(120000).sync

destroy_command_exitcode = 0
destroy_command_message = "success"

if destroy_command_exitcode == 0
	@log.info("Success in executing #{@ssh_connector_name} connector, where exitcode :: #{destroy_command_exitcode} |
                                                        message :: #{destroy_command_message}")
	destroy_command_response_body = %x[ #{@destroy_command} ]
	if destroy_command_response_body.include? "Apply complete!"
		if destroy_command_response_body.include? "1 destroyed"
			@log.info("VM destroyed successfully..")
			@output.set("exitcode",0)
			@output.set("message","Success")
		else
			@log.info("VM already destroyed..")
			@output.set("exitcode",0)
			@output.set("message","VM already destroyed")
		end
	else
		@log.error("Error occured while destroying VM : #{destroy_command_response_body}")
		@output.set("exitcode",1)
		@output.set("error",destroy_command_response_body)
	end
else
	@log.error("Failure in executing #{@ssh_connector_name} connector where, exitcode : #{destroy_command_exitcode} |
                                                         message : #{destroy_command_message}")
	error_body = "Job-id : #{@input.jobid()} | Error occured while destroying : #{destroy_command_message}"
	@output.set("exitcode",1)
  	@output.set("error",error_body)
end
@log.trace("Finished execution of flint-terraform:common:destroy.rb flintbit..")
